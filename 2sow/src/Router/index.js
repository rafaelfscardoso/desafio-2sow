import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import OpeningPage from '../Pages/OpeningPage';
import LoginPage from '../Pages/LoginPage';
import HomePage from '../Pages/HomePage';
import CreateUserPage from '../Pages/CreateUserPage';

const Router = () => {
  
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path='/' component={OpeningPage} />
        <Route exact path='/login' component={LoginPage} />
        <Route exact path='/home' component={HomePage} />
        <Route exact path='/user/create' component={CreateUserPage} />
      </Switch>
    </BrowserRouter>
  );
};

export default Router;