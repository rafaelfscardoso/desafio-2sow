import React from 'react';

import Router from './Router';

import { AppContainer } from './style';

const App = () => {

  return (
    <AppContainer>
      <Router />
    </AppContainer>
  );
};

export default App;
