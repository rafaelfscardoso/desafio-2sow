import axios from 'axios';

const baseUrl = 'http://localhost:5000/usuarios'

export const getUsers = async () => {
  const response = await axios.get(baseUrl);
  return response.data;
}

export const getUsersByQuery = async (query) => {
  const response = await axios.get(`${baseUrl}?nome_like=${query}`);
  return response.data;
}

export const createUser = async (body) => {
  const response = await axios.post(baseUrl, body);
  return response.data;
}

export const editUser = async (body, userId) => {
  const response = await axios.put(`${baseUrl}/${userId}`, body);
  return response.data;
}

export const deleteUser = async (userId) => {
  const response = await axios.delete(`${baseUrl}/${userId}`);
  return response.data;
}

export const getAddressByCep = async (cep) => {
  const response = await axios.get(`http://viacep.com.br/ws/${cep}/json/`);
  return response.data;
}