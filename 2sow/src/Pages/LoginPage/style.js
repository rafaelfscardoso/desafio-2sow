import styled from 'styled-components';
import { Form } from 'semantic-ui-react';

export const LoginPageContainer = styled.main`
  grid-row: 2 / span 1;
  height: 100%;
  display: grid;
  place-items: center;
  @media screen and (min-width: 560px) {
    width: 90%;
    margin: 0 auto;
  }
  @media screen and (max-width: 560px) {
    width: 100%;
    padding: 1rem;
  }
`
export const LoginForm = styled(Form)`
  display: flex;
  flex-direction: column;
  @media screen and (max-width: 560px) {
    width: 100%;
  }
  @media screen and (min-width: 560px) {
    width: 460px;
  }
`