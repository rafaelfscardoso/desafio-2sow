import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Input, Button, Icon } from 'semantic-ui-react';

import { useForm, usePublicPage } from '../../hooks';
import { PageContainer } from '../../style';

import { LoginPageContainer, LoginForm } from './style';

const LoginPage = () => {

  usePublicPage();

  const history = useHistory();

  const { form, onChange, resetForm } = useForm({
    email: '',
    password: ''
  });

  const [emailIsInvalid, setEmailIsInvalid] = useState(false);
  const [passwordIsInvalid, setPasswordIsInvalid] = useState(false);
  const [passwordIsShowed, setPasswordIsShowed] = useState(false);

  const { email, password } = form;

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    onChange(name, value);
  };

  const checkEmail = (email) => {
    const rgx = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/i;
    return rgx.test(email.toLowerCase());
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (checkEmail(email)) {
      if (password.length > 3) {
        resetForm();
        window.localStorage.setItem('token', 'token');
        history.push('/home');
      } else {
        setPasswordIsInvalid(true);
      }
    } else {
      setEmailIsInvalid(true);
    }
  }
  
  return (
    <PageContainer>
      <LoginPageContainer>
        <h3>Entrar</h3>
        <LoginForm onSubmit={handleSubmit} >
          <LoginForm.Field
            required
            control={Input}
            name='email'
            type='email'
            value={email}
            label='Email'
            placeholder='exemplo@exemplo.com'
            onChange={handleInputChange}
            error={emailIsInvalid && { content: 'Favor insira um email válido', pointing: 'below' }}
          />
          <LoginForm.Field
            required
            control={Input}
            name='password'
            type={passwordIsShowed ? 'text' : 'password'}
            value={password}
            label='Senha'
            placeholder='Mínimo 4 caracteres'
            onChange={handleInputChange}
            error={passwordIsInvalid && 'A senha deve ter ao menos 4 caracteres'}
            icon={
              <Icon 
                circular
                inverted
                link
                data-testid='showPassword-icon'
                name={passwordIsShowed ? 'eye' : 'eye slash'}
                onClick={() => setPasswordIsShowed(!passwordIsShowed)}
              />
            }
          />
          <LoginForm.Field 
            fluid
            color='orange'
            control={Button}
            type='submit'
            content='Confirmar' 
          />
        </LoginForm>
      </LoginPageContainer>
    </PageContainer>
  );
};

export default LoginPage