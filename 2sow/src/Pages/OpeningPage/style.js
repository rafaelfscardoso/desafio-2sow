import styled from 'styled-components';

export const OpeningPageContainer = styled.main`
  grid-row: 1 / span 3;
  background-color: orange;
  display: grid;
  place-items: center;
  color: whitesmoke;
`