import React from 'react';
import { useHistory } from 'react-router-dom';

import { PageContainer } from '../../style';

import { OpeningPageContainer } from './style';

const OpeningPage = () => {

  const history = useHistory();

  const goToLoginPage = () => {
    setTimeout(() => {
      history.push('/login');
    }, 4000);
  }

  return (
    <PageContainer>
      <OpeningPageContainer>
        <h1>UPDAATER</h1>
        {goToLoginPage()}
      </OpeningPageContainer>
    </PageContainer>
  );
};

export default OpeningPage;