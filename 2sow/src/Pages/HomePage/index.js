import React, { useState, useEffect } from 'react';
import { Input, Icon } from 'semantic-ui-react';

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import EditUser from '../../components/EditUser';
import UserInfo from '../../components/UserInfo';

import { usePrivatePage, useForm } from '../../hooks';
import { getUsers, getUsersByQuery } from '../../requests';
import { PageContainer } from '../../style';

import { HomePageContainer, HomeForm } from './style';

const HomePage = () => {

  usePrivatePage();

  const [refresh, setRefresh] = useState(false);

  const [userList, setUserList] = useState(undefined);
  const [userToEdit, setUserToEdit] = useState(undefined);
  const [showEditUser, setShowEditUser] = useState(false);
  const [loading, setLoading] = useState(false);

  const { form, onChange, resetForm } = useForm({ search: '' });

  const { search } = form;

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    onChange(name, value);
  };

  const getUserList = async () => {
    try {
      const response = await getUsers();
      setUserList(response);
    } catch (error) {
      console.error(error.response);
    };
  };

  const queryUserList = async () => {
    try {
      setLoading(true);
      const response = await getUsersByQuery(search);
      setUserList(response);
      setLoading(false);
    } catch (error) {
      console.error(error.response);
    };
  };

  useEffect(() => {
    getUserList();
  }, [setUserList, setLoading, refresh]);

  const handleSubmit = (event) => {
    event.preventDefault();
    setUserList(undefined);
    queryUserList();
  };

  const editUser = (user) => {
    setUserToEdit(user);
    setShowEditUser(true);
  };

  const clearSearch = () => {
    resetForm();
    setUserList(undefined);
    getUserList();
  };

  return (
    <PageContainer>
      <Header />
      <HomePageContainer>
        {!showEditUser && (
          <HomeForm onSubmit={handleSubmit} >
            <HomeForm.Field
              control={Input}
              loading={loading}
              name='search'
              type='text'
              value={search}
              placeholder='Pesquise por nome aqui'
              onChange={handleInputChange}
              icon={search && (
                <Icon 
                  link
                  data-testid='clearInput-icon'
                  name='remove circle'
                  onClick={clearSearch}
                />
              )}
              action={{ icon: 'search' }}
              actionPosition='left'
            />
          </HomeForm>
        )}
        {userList ? (
          <div>
            {showEditUser && <span onClick={() => setShowEditUser(false)} >Voltar</span>}
            {showEditUser ? (
              <EditUser user={userToEdit} setEdit={setShowEditUser} refresh={{ refresh, setRefresh }} />
            ) : (
              <UserInfo users={userList} edit={editUser} refresh={{ refresh, setRefresh }} />
            )}
          </div>
        ) : <div>Carregando...</div>}
      </HomePageContainer>
      <Footer />
    </PageContainer>
  );
};

export default HomePage;