import styled from 'styled-components';
import { Form } from 'semantic-ui-react';

export const HomePageContainer = styled.main`
  grid-row: 2 / span 1;
  height: 100%;
  padding: 1rem 0;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  > div {
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    > span {
      cursor: pointer;
      align-self: flex-start;
      padding: .5em;
      border: 1px solid white;
      :hover {
        background-color: orange;
        border: 1px solid #f7f7f7;
        border-radius: .5rem;
      }
    }
  }
  @media screen and (min-width: 560px) {
    width: 90%;
    margin: 0 auto;
  }
  @media screen and (max-width: 560px) {
    width: 100%;
    padding: 1rem;
    > div > span {
      margin-bottom: 1rem;
      background-color: orange;
      border: 1px solid #f7f7f7;
      border-radius: .5rem;
    }
  }
`
export const HomeForm = styled(Form)`
  width: 100%;
  display: flex;
  flex-direction: column;
  margin-bottom: 1rem;
  @media screen and (max-width: 560px) {}
  @media screen and (min-width: 560px) {
    width: 460px;
  }
`