import React, { useState } from 'react';

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Form from '../../components/Form';

import { useForm, usePrivatePage } from '../../hooks';
import { createUser } from '../../requests';
import { PageContainer } from '../../style';

import { CreateUserPageContainer } from './style';

const CreateUserPage = () => {

  usePrivatePage();

  const { form, onChange, resetForm } = useForm({
    name: '',
    cpf: '',
    email: '',
    zipCode: '',
    number: ''
  });

  const initialAddress = {
    street: '',
    neighborhood: '',
    city: ''
  }

  const [address, setAddress] = useState(initialAddress);

  const resetAddress = () => setAddress(initialAddress);

  return (
    <PageContainer>
      <Header />
      <CreateUserPageContainer>
        <Form id={0} form={{ form, onChange, resetForm }} address={{ address, setAddress, resetAddress }} submit={createUser} />
      </CreateUserPageContainer>
      <Footer />
    </PageContainer>
  );
};

export default CreateUserPage;