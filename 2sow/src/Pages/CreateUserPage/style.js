import styled from 'styled-components';

export const CreateUserPageContainer = styled.main`
  grid-row: 2 / span 1;
  height: 100%;
  padding: 1rem;
  display: grid;
  place-items: center;
  @media screen and (min-width: 560px) {
    width: 90%;
    margin: 0 auto;
  }
  @media screen and (max-width: 560px) {
    width: 100%;
  }
`