import styled from 'styled-components';

export const AppContainer = styled.div`
  height: 100vh;
  width: 100vw;
`

export const PageContainer = styled.div`
  min-height: 100vh;
  width: 100vw;
  display: grid;
  @media screen and (max-width: 560px) {
    grid-template-rows: 4rem 1fr 4rem;
  }
  @media screen and (min-width: 560px) {
    grid-template-rows: 100px 1fr 100px;
  }
`