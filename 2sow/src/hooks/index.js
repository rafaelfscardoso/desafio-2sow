import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

export const useForm = (initialValues) => {

  const [form, setForm] = useState(initialValues);

  const onChange = (name, value) => {
    const newForm = { ...form, [name]: value };
    setForm(newForm);
  };

  const resetForm = () => {
    setForm(initialValues);
  };

  return { form, setForm, onChange, resetForm };
};

export const usePrivatePage = () => {

  const history = useHistory();

  useEffect(() => {
    const token = window.localStorage.getItem('token');

    if (!token) {
      history.push('/login');
    };
  }, [history]);
};

export const usePublicPage = () => {

  const history = useHistory();

  useEffect(() => {
    const token = window.localStorage.getItem('token');

    if (token) {
      history.push('/home');
    }
  }, [history]);
};