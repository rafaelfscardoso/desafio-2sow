import styled from 'styled-components';
import { Form } from 'semantic-ui-react';

export const FormForm = styled(Form)`
  display: flex;
  flex-direction: column;
  @media screen and (max-width: 560px) {
    width: 100%;
  }
  @media screen and (min-width: 560px) {
    width: 460px;
  }
`