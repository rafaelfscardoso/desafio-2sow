import React, { useState } from 'react';
import { Input, Button } from 'semantic-ui-react';

import { getAddressByCep } from '../../requests';

import { FormForm } from './style';

const Form = (props) => {

  const [emailIsInvalid, setEmailIsInvalid] = useState(false);
  const [zipCodeIsInvalid, setZipCodeIsInvalid] = useState(false);
  const [loading, setLoading] = useState(false);

  const { form, onChange, resetForm } = props.form;
  const { address, setAddress, resetAddress } = props.address;
  const { submit } = props;
  const { id } = props;

  const { name, cpf, email, zipCode, number } = form;
  const { street, neighborhood, city } = address;

  const cpfMask = (value) => {
    return value
      .replace(/\D/g, '') 
      .replace(/(\d{3})(\d)/, '$1.$2') 
      .replace(/(\d{3})(\d)/, '$1.$2')
      .replace(/(\d{3})(\d{1,2})/, '$1-$2')
      .replace(/(-\d{2})\d+?$/, '$1');
  };

  const zipCodeMask = (value) => {
    return value
      .replace(/\D/g, '')
      .replace(/(\d{5})(\d)/,'$1-$2')
      .replace(/(-\d{3})\d+?/, '$1');
  };
  
  const zipCodeUnmask = (value) => {
    return value.split('-').join('');
  };

  const checkEmail = (email) => {
    const rgx = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/i;
    return rgx.test(email.toLowerCase());
  };

  const fillOutAddress = (address) => {
    const { logradouro, bairro, localidade } = address;
    setAddress({
      street: logradouro,
      neighborhood: bairro,
      city: localidade
    });
  };

  const handleInputChange = async (event) => {
    const { name, value } = event.target;

    if (name === 'cpf') {
      onChange(name, cpfMask(value));
    } else if (name === 'zipCode') {
      onChange(name, zipCodeMask(value));

      if (value.length === 9) {
        const response = await getAddressByCep(zipCodeUnmask(value));
        if (response.erro) {
          setZipCodeIsInvalid(true);
        } else {  
          fillOutAddress(response);
        }
      }
    } else {
      onChange(name, value);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (!checkEmail(email)) {
      setEmailIsInvalid(true);
      return;
    }
    const endereco = { 
      cep: zipCodeUnmask(zipCode), 
      rua: street, 
      numero: number, 
      bairro: neighborhood, 
      cidade: city 
    };
    const body = { nome: name, cpf, email, endereco };
    try {
      setLoading(true);
      if (id) {
        const { setEdit } = props;
        const { refresh, setRefresh } = props.refresh;
        await submit(body, id);
        setEdit(false);
        setRefresh(!refresh);
      } else {
        await submit(body);
        resetForm();
        resetAddress();
        setEmailIsInvalid(false);
        setZipCodeIsInvalid(false);
        setLoading(false);
      }
    } catch (error) {
      console.error(error.response);
    }
  }

  return (
    <FormForm loading={loading} onSubmit={handleSubmit} >
      <FormForm.Field
        required
        control={Input}
        name='name'
        type='text'
        value={name}
        label='Nome'
        placeholder='Nome'
        onChange={handleInputChange}
      />
      <FormForm.Field
        required
        control={Input}
        name='cpf'
        type='text'
        value={cpf}
        label='CPF'
        placeholder='000.000.000-00'
        onChange={handleInputChange}
        maxLength={14}
      />
      <FormForm.Field 
        required
        control={Input}
        name='email'
        type='email'
        value={email}
        label='Email'
        placeholder='exemplo@exemplo.com'
        onChange={handleInputChange}
        error={emailIsInvalid && 'Favor insira um email válido'}
      />
      <FormForm.Field
        required
        control={Input}
        name='zipCode'
        type='text'
        value={zipCode}
        label='CEP'
        placeholder='00000-000'
        onChange={handleInputChange}
        maxLength={9}
        error={zipCodeIsInvalid && 'Favor insira um cep válido'}
      />
      <FormForm.Field 
        required
        control={Input}
        name='street'
        type='text'
        value={street}
        label='Logradouro'
        placeholder='Rua / Av.'
        readOnly
      />
      <FormForm.Field 
        required
        control={Input}
        name='number'
        type='number'
        value={number}
        label='Número'
        placeholder='Número'
        onChange={handleInputChange}
      />
      <FormForm.Field
        required
        control={Input}
        name='neighborhood'
        type='text'
        value={neighborhood}
        label='Bairro'
        placeholder='Bairro'
        readOnly
      />
      <FormForm.Field
        required
        control={Input}
        name='city'
        type='text'
        value={city}
        label='Cidade'
        placeholder='Cidade'
        readOnly
      />
      <FormForm.Field 
        fluid
        color='orange'
        control={Button}
        type='submit'
        content='Salvar' 
      />
    </FormForm>
  );
};

export default Form;