import styled from 'styled-components';

export const TableContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  @media screen and (min-width: 560px) {}
  @media screen and (max-width: 560px) {}
`