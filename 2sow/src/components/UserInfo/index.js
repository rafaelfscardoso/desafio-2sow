import React, { useState } from 'react';
import { Table, Icon } from 'semantic-ui-react';

import { deleteUser } from '../../requests';

import { TableContainer } from './style';

const UserInfo = (props) => {

  const [loading, setLoading] = useState(false);

  const { users, edit } = props;

  const { refresh, setRefresh } = props.refresh;

  const delUser = async (userId) => {
    try {
      setLoading(true);
      await deleteUser(userId);
      setRefresh(!refresh);
      setLoading(false);
    } catch (error) {
      console.error(error.response);
    }
  }

  return (
    <TableContainer>
      {users.length ?
        <Table striped color='orange' >
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell content='Nome' />
              <Table.HeaderCell content='CPF' />
              <Table.HeaderCell content='Email' />
              <Table.HeaderCell content='Cidade' />
              <Table.HeaderCell />
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {users.map(user => {
              const { id, nome, cpf, email, endereco } = user;
              const { cidade } = endereco;
              return (
                <Table.Row key={id} >
                  <Table.Cell content={nome} />
                  <Table.Cell content={cpf} />
                  <Table.Cell content={email} />
                  <Table.Cell content={cidade} />
                  <Table.Cell collapsing >
                    <Icon link size='large' name='edit' data-testid='edit-icon' onClick={() => edit(user)} />
                    <Icon link size='large' name='trash' data-testid='trash-icon' onClick={() => delUser(id)} loading={loading} />
                  </Table.Cell>
                </Table.Row>
              );
            })}
          </Table.Body>
        </Table>
      : <div>Não encontramos nenhum :(</div>}
    </TableContainer>
  );
};

export default UserInfo;