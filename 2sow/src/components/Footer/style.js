import styled from 'styled-components';

export const FooterContainer = styled.footer`
  width: 100%;
  height: 100%;
  grid-row: 3 / span 1;
  background-color: orange;
  display: flex;
  align-items: center;
  @media screen and (max-width: 420px) {
    > div {
      width: 100%;
      margin: 0 1rem;
    }
  }
  @media screen and (min-width: 420px) {
    > div {
      width: 90%;
      margin: 0 auto;
    }
  }
`