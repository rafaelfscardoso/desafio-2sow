import React from 'react';

import {
  FooterContainer
} from './style';

const Footer = () => {

  return (
    <FooterContainer>
      <div>
        <p>{`© 2020 - ${(new Date()).getFullYear()}, Todos os direitos reservados`}</p>
      </div>
    </FooterContainer>
  );
};

export default Footer;