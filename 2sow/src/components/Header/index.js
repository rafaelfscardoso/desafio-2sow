import React from 'react';
import { useHistory } from 'react-router-dom';
import { Menu, Dropdown } from 'semantic-ui-react';

import { 
  HeaderContainer, 
  HeaderNavDesktop, 
  HeaderNavMobile
} from './style';

const Header = () => {

  const history = useHistory();

  const goToHome = () => {
    history.push('/home');
  };

  const goToCreateUser = () => {
    history.push('/user/create');
  };

  const submitLogOut = () => {
    window.localStorage.removeItem('token');
    history.push('/login');
  };

  return (
    <HeaderContainer>
      <div>
        <span>
          <h3>UPDAATER</h3>
        </span>
        <HeaderNavDesktop>
          <span onClick={goToHome} >Home</span>
          <span onClick={goToCreateUser} >Formulário</span>
          <span onClick={submitLogOut} >Sair</span>
        </HeaderNavDesktop>
        <HeaderNavMobile>
          <Menu color='orange' inverted >
            <Menu.Menu position='right' >
              <Dropdown item simple icon='bars' direction='right' data-testid='menu-icon' >
                <Dropdown.Menu>
                  <Dropdown.Item onClick={goToHome} >Home</Dropdown.Item>
                  <Dropdown.Item onClick={goToCreateUser} >Formulário</Dropdown.Item>
                  <Dropdown.Divider />
                  <Dropdown.Item onClick={submitLogOut} >Sair</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Menu.Menu>
          </Menu>
        </HeaderNavMobile>
      </div>
    </HeaderContainer>
  );
};

export default Header;