import styled from 'styled-components';

export const HeaderContainer = styled.header`
  grid-column: 1 / span 1;
  height: 100%;
  width: 100%;
  background-color: orange;
  box-shadow: 0 0.5px 0 0 rgba(0, 0, 0, 0.25);
  > div {
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: 100%;
    @media screen and (min-width: 560px) {
      width: 90%;
      margin: 0 auto;
    }
    @media screen and (max-width: 560px) {
      padding: 1rem;
    }
  }
`
export const HeaderNavDesktop = styled.nav`
  @media screen and (min-width: 560px) {
    > span {
      cursor: pointer;
      padding: .5rem;
      border-radius: .5rem;
      margin-left: .5rem;
      :last-of-type {
        background-color: darkorange;
        margin-left: 1rem;
        :hover {
          background-color: whitesmoke;
          color: darkorange;
        }
      }
      :hover {
        background-color: darkorange;
      }
    }
  }
  @media screen and (max-width: 560px) {
    display: none;
  }
`
export const HeaderNavMobile = styled.div`
  @media screen and (min-width: 560px) {
    display: none;
  }
  @media screen and (max-width: 560px) {}
`
