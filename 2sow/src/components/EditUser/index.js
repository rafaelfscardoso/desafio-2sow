import React, { useState } from 'react';

import Form from '../Form';

import { useForm } from '../../hooks';
import { editUser } from '../../requests';

const EditUser = (props) => {

  const { id, nome, cpf, email, endereco } = props.user;
  const { cep, rua, numero, bairro, cidade } = endereco;
  const { setEdit, refresh } = props;

  const { form, onChange, resetForm } = useForm({  
    name: nome, 
    cpf, 
    email, 
    zipCode: cep, 
    number: numero 
  });

  const initialAddress = {
    street: rua,
    neighborhood: bairro,
    city: cidade
  }

  const [address, setAddress] = useState(initialAddress);

  const resetAddress = () => setAddress(initialAddress);

  return (
    <Form 
      id={id}
      form={{ form, onChange, resetForm }} 
      address={{ address, setAddress, resetAddress }} 
      submit={editUser} 
      setEdit={setEdit}
      refresh={refresh} 
    />
  );
};

export default EditUser;