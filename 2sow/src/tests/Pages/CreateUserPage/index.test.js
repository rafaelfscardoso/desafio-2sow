import React from 'react';
import { MemoryRouter } from "react-router-dom";
import { findByTestId, render, wait } from '@testing-library/react';
import userEvent from "@testing-library/user-event";
import axios from 'axios';

import CreateUserPage from '../../../Pages/CreateUserPage';

axios.get = jest.fn().mockResolvedValue('');
axios.delete = jest.fn().mockResolvedValue('');
axios.put = jest.fn().mockResolvedValue('');

const mockHistoryPush = jest.fn();

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe('Initial render, ', () => {

  Object.defineProperty(window, 'localStorage', {
    value: {
      getItem: jest.fn(() => null),
      setItem: jest.fn(() => null),
      removeItem: jest.fn(() => null)
    },
    writable: true
  });

  test('if localStorage has no token, must go to LoginPage', () => {
    render(<CreateUserPage />);

    expect(window.localStorage.getItem).toHaveBeenCalledTimes(1);
    expect(mockHistoryPush).toHaveBeenCalledWith('/login');

  });

  test('must show Header info', () => {
    const { getAllByText, getByTestId } = render(<CreateUserPage />);

    const homeNav = getAllByText(/home/i);
    const formNav = getAllByText(/form/i);
    const logoutNav = getAllByText(/sair/i);

    expect(homeNav).toHaveLength(2);
    expect(formNav).toHaveLength(2);
    expect(logoutNav).toHaveLength(2);

    const menuIcon = getByTestId('menu-icon');

    expect(menuIcon).toBeInTheDocument();
  });

  test('must have name input', () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    expect(getByPlaceholderText('Nome')).toBeInTheDocument();
  });

  test('must have cpf input', () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    expect(getByPlaceholderText('000.000.000-00')).toBeInTheDocument();
  });

  test('must have email input', () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    expect(getByPlaceholderText('exemplo@exemplo.com')).toBeInTheDocument();
  });

  test('must have zipCode input', () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    expect(getByPlaceholderText('00000-000')).toBeInTheDocument();
  });

  test('must have street input', () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    expect(getByPlaceholderText('Rua / Av.')).toBeInTheDocument();
  });

  test('must have number input', () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    expect(getByPlaceholderText('Número')).toBeInTheDocument();
  });

  test('must have neighborhood input', () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    expect(getByPlaceholderText('Bairro')).toBeInTheDocument();
  });

  test('must have city input', () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    expect(getByPlaceholderText('Cidade')).toBeInTheDocument();
  });
});

describe('When interact with nav menu', () => {

  beforeEach(() => {
    Object.defineProperty(window, 'localStorage', {
      value: {
        getItem: jest.fn(() => 'token'),
        setItem: jest.fn(() => null),
        removeItem: jest.fn(() => null)
      },
      writable: true
    });
  });

  test(' and click Home', () => {
    const { getAllByText } = render(
      <MemoryRouter>
        <CreateUserPage />
      </MemoryRouter>
    );

    const homeNav = getAllByText(/home/i);

    userEvent.click(homeNav[0]);
    expect(mockHistoryPush).toHaveBeenCalledWith('/home');
  });

  test(' and click Sair', () => {
    const { getAllByText } = render(
      <MemoryRouter>
        <CreateUserPage />
      </MemoryRouter>
    );

    const logoutNav = getAllByText(/sair/i);

    userEvent.click(logoutNav[0]);
    expect(window.localStorage.removeItem).toHaveBeenCalled();
    expect(mockHistoryPush).toHaveBeenCalledWith('/login');
  });
});

describe('When type', () => {
  test('name must change', async () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    const nameInput = getByPlaceholderText('Nome');

    await userEvent.type(nameInput, 'foo bar');

    expect(nameInput).toHaveValue('foo bar');
  });

  test('cpf must change and implement mask', async () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    const cpfInput = getByPlaceholderText('000.000.000-00');

    await userEvent.type(cpfInput, '21312312345');

    expect(cpfInput).toHaveValue('213.123.123-45');
  });

  test('email must change', async () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    const emailInput = getByPlaceholderText('exemplo@exemplo.com');

    await userEvent.type(emailInput, 'foo_bar@email.com');

    expect(emailInput).toHaveValue('foo_bar@email.com');
  });

  test('zip code must change and implement mask', async () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    const zipCodeInput = getByPlaceholderText('00000-000');

    await userEvent.type(zipCodeInput, '13454000');

    expect(zipCodeInput).toHaveValue('13454-000');
  });

  test('street mustn\'t change', async () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    const streetInput = getByPlaceholderText('Rua / Av.');

    await userEvent.type(streetInput, 'rua feliz');

    expect(streetInput).not.toHaveValue('rua feliz');
    expect(streetInput).toHaveValue('');
  });

  test('number must change', async () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    const numberInput = getByPlaceholderText('Número');

    await userEvent.type(numberInput, '785');

    expect(numberInput).toHaveValue(785);
  });

  test('neighborhood mustn\'t change', async () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    const neighborhoodInput = getByPlaceholderText('Bairro');

    await userEvent.type(neighborhoodInput, 'bairro azul');

    expect(neighborhoodInput).not.toHaveValue('bairro azul');
    expect(neighborhoodInput).toHaveValue('');
  });

  test('city mustn\'t change', async () => {
    const { getByPlaceholderText } = render(<CreateUserPage />);

    const cityInput = getByPlaceholderText('Cidade');

    await userEvent.type(cityInput, 'cidade eterna');

    expect(cityInput).not.toHaveValue('cidade eterna');
    expect(cityInput).toHaveValue('');
  });

  test('zip code and it\'s invalid, must appear an error', async () => {
    axios.get = jest.fn().mockResolvedValue({
      data: { erro: true }
    });
    const { getByPlaceholderText, findByText } = render(<CreateUserPage />);

    const zipCodeInput = getByPlaceholderText('00000-000');

    await userEvent.type(zipCodeInput, '13454-000');

    await wait(expect(axios.get).toHaveBeenCalledWith('http://viacep.com.br/ws/13454000/json/'));

    expect(await findByText('Favor insira um cep válido')).toBeInTheDocument();
  });

  test('zip code and it\'s valid, must appear street, neighborhood and city filled out', async () => {
    axios.get = jest.fn().mockResolvedValue({
      data: {
        logradouro: 'rua feliz',
        bairro: 'bairro azul',
        localidade: 'cidade eterna'
      }
    });
    const { getByPlaceholderText } = render(<CreateUserPage />);

    const zipCodeInput = getByPlaceholderText('00000-000');

    await userEvent.type(zipCodeInput, '13454-000');

    await wait(expect(axios.get).toHaveBeenCalledWith('http://viacep.com.br/ws/13454000/json/'));

    expect(getByPlaceholderText('Rua / Av.')).toHaveValue('rua feliz');
    expect(getByPlaceholderText('Bairro')).toHaveValue('bairro azul');
    expect(getByPlaceholderText('Cidade')).toHaveValue('cidade eterna');
  });
});

describe('When submit form ', () => {
  test('and email is invalid, must appear an error', async () => {
    axios.get = jest.fn().mockResolvedValue({
      data: {
        logradouro: 'rua feliz',
        bairro: 'bairro azul',
        localidade: 'cidade eterna'
      }
    });
    axios.post = jest.fn().mockResolvedValue();
    const { getByPlaceholderText, getByText } = render(<CreateUserPage />);

    const nameInput = getByPlaceholderText('Nome');
    const cpfInput = getByPlaceholderText('000.000.000-00');
    const emailInput = getByPlaceholderText('exemplo@exemplo.com')
    const zipCodeInput = getByPlaceholderText('00000-000');
    const numberInput = getByPlaceholderText('Número');

    await userEvent.type(nameInput, 'foo bar');
    await userEvent.type(cpfInput, '213.123.123-45');
    await userEvent.type(emailInput, 'foo_bar@email');
    await userEvent.type(zipCodeInput, '13454-000');
    await userEvent.type(numberInput, '785');

    await wait(axios.get);

    userEvent.click(getByText('Salvar'));

    expect(getByText('Favor insira um email válido')).toBeInTheDocument();
    await wait(expect(axios.post).not.toHaveBeenCalled());
  });

  test('and all inputs are valid, after it inputs must be clear', async () => {
    axios.get = jest.fn().mockResolvedValue({
      data: {
        logradouro: 'rua feliz',
        bairro: 'bairro azul',
        localidade: 'cidade eterna'
      }
    });
    axios.post = jest.fn().mockResolvedValue();
    const { getByPlaceholderText, getByText } = render(<CreateUserPage />);

    const nameInput = getByPlaceholderText('Nome');
    const cpfInput = getByPlaceholderText('000.000.000-00');
    const emailInput = getByPlaceholderText('exemplo@exemplo.com')
    const zipCodeInput = getByPlaceholderText('00000-000');
    const numberInput = getByPlaceholderText('Número');

    await userEvent.type(nameInput, 'foo bar');
    await userEvent.type(cpfInput, '213.123.123-45');
    await userEvent.type(emailInput, 'foo_bar@email.com');
    await userEvent.type(zipCodeInput, '13454-000');
    await userEvent.type(numberInput, '785');

    await wait(axios.get);

    userEvent.click(getByText('Salvar'));

    await wait(expect(axios.post).toHaveBeenCalledWith('http://localhost:5000/usuarios', {
      nome: 'foo bar',
      cpf: '213.123.123-45',
      email: 'foo_bar@email.com',
      endereco: {
        cep: '13454000',
        rua: 'rua feliz',
        numero: '785',
        bairro: 'bairro azul',
        cidade: 'cidade eterna'
      }
    }));
  });
});