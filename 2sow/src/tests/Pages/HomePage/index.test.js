import React from 'react';
import { MemoryRouter } from "react-router-dom";
import { render, wait } from '@testing-library/react';
import userEvent from "@testing-library/user-event";
import axios from 'axios';

import HomePage from '../../../Pages/HomePage';

axios.get = jest.fn().mockResolvedValue('');
axios.delete = jest.fn().mockResolvedValue('');
axios.put = jest.fn().mockResolvedValue('');

const mockHistoryPush = jest.fn();

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe('Initial render, ', () => {

  Object.defineProperty(window, 'localStorage', {
    value: {
      getItem: jest.fn(() => null),
      setItem: jest.fn(() => null),
      removeItem: jest.fn(() => null)
    },
    writable: true
  });

  test('if localStorage has no token, must go to LoginPage', () => {
    render(<HomePage />);

    expect(window.localStorage.getItem).toHaveBeenCalledTimes(1);
    expect(mockHistoryPush).toHaveBeenCalledWith('/login');

  });

  test('must show Header info', () => {
    const { getAllByText, getByTestId } = render(<HomePage />);

    const homeNav = getAllByText(/home/i);
    const formNav = getAllByText(/form/i);
    const logoutNav = getAllByText(/sair/i);

    expect(homeNav).toHaveLength(2);
    expect(formNav).toHaveLength(2);
    expect(logoutNav).toHaveLength(2);

    const menuIcon = getByTestId('menu-icon');

    expect(menuIcon).toBeInTheDocument();
  });

  test('must have search input', () => {
    const { getByPlaceholderText } = render(<HomePage />);

    const searchInput = getByPlaceholderText(/pesquise/i);

    expect(searchInput).toBeInTheDocument();
  });

  test('must have a icon button search', () => {
    const { getByRole } = render(<HomePage />);

    const iconButton = getByRole(/button/i);

    expect(iconButton).toHaveClass('icon button');
  });

  test('must show \'Carregando...\' before axios.get', () => {
    const { getByText } = render(<HomePage />);

    expect(getByText(/carregando/i)).toBeInTheDocument();
  });

  test('must render table after axios.get', async () => {
    axios.get = jest.fn().mockResolvedValue({
      data: [
        { 
          id: '1',
          nome: 'foo bar',
          cpf: '213.123.123-45',
          email: 'foo_bar@email.com',
          endereco: {
            cep: '13454000',
            rua: 'rua talvez',
            numero: '785',
            bairro: 'bairro azul',
            cidade: 'cidade eterna',
          }
        }
      ]
    });
    const { findByText, findByTestId } = render(<HomePage />);

    expect(axios.get).toHaveBeenCalled();
    expect(await findByText('foo bar')).toBeInTheDocument();
    expect(await findByText('213.123.123-45')).toBeInTheDocument();
    expect(await findByText('foo_bar@email.com')).toBeInTheDocument();
    expect(await findByText('cidade eterna')).toBeInTheDocument();
    expect(await findByTestId('edit-icon')).toBeInTheDocument();
    expect(await findByTestId('trash-icon')).toBeInTheDocument();
  });
});

describe('When interact with nav menu', () => {

  beforeEach(() => {
    Object.defineProperty(window, 'localStorage', {
      value: {
        getItem: jest.fn(() => 'token'),
        setItem: jest.fn(() => null),
        removeItem: jest.fn(() => null)
      },
      writable: true
    });
  });

  test(' and click Formulário', () => {
    const { getAllByText } = render(
      <MemoryRouter>
        <HomePage />
      </MemoryRouter>
    );

    const formNav = getAllByText(/form/i);

    userEvent.click(formNav[0]);
    expect(mockHistoryPush).toHaveBeenCalledWith('/user/create');
  });

  test(' and click Sair', () => {
    const { getAllByText } = render(
      <MemoryRouter>
        <HomePage />
      </MemoryRouter>
    );

    const logoutNav = getAllByText(/sair/i);

    userEvent.click(logoutNav[0]);
    expect(window.localStorage.removeItem).toHaveBeenCalled();
    expect(mockHistoryPush).toHaveBeenCalledWith('/login');
  });
});

describe('When type ', () => {

  test('search must change and appear clear input icon', async () => {
    const { getByPlaceholderText, findByTestId } = render(<HomePage />);

    const searchInput = getByPlaceholderText(/pesquise/i);

    await userEvent.type(searchInput, 'foo');

    expect(searchInput).toHaveValue('foo');
    expect(await findByTestId('clearInput-icon')).toBeInTheDocument();
  });

  test('and then click clear input icon', async () => {
    const { getByPlaceholderText, getByText, findByTestId } = render(<HomePage />);

    const searchInput = getByPlaceholderText(/pesquise/i);

    await userEvent.type(searchInput, 'foo');

    const clearInputIcon = await findByTestId('clearInput-icon');

    userEvent.click(clearInputIcon);

    expect(searchInput).toHaveValue('');
    expect(getByText('Carregando...')).toBeInTheDocument();
    expect(axios.get).toHaveBeenCalled();
  });
});

describe('When submit search ', () => {

  test('and returns an empty array', async () => {
    axios.get = jest.fn().mockResolvedValue({
      data: []
    });
    const { getByPlaceholderText, getByRole, getByText, findByText } = render(<HomePage />);

    const searchInput = getByPlaceholderText(/pesquise/i);
    const iconButton = getByRole(/button/i);

    await userEvent.type(searchInput, 'ffo');
    userEvent.click(iconButton);

    expect(getByText('Carregando...')).toBeInTheDocument();
    await wait(expect(axios.get).toHaveBeenCalledWith('http://localhost:5000/usuarios?nome_like=ffo'));
    expect(await findByText('Não encontramos nenhum :(')).toBeInTheDocument();
  });

  test('and returns a successfull response', async () => {
    axios.get = jest.fn().mockResolvedValue({
      data: [
        { 
          id: '1',
          nome: 'foo bar',
          cpf: '213.123.123-45',
          email: 'foo_bar@email.com',
          endereco: {
            cep: '13454000',
            rua: 'rua talvez',
            numero: '785',
            bairro: 'bairro azul',
            cidade: 'cidade eterna',
          }
        }
      ]
    });
    const { getByPlaceholderText, getByRole, findByText, findByTestId } = render(<HomePage />);

    const searchInput = getByPlaceholderText(/pesquise/i);
    const iconButton = getByRole(/button/i);

    await userEvent.type(searchInput, 'foo');

    userEvent.click(iconButton);

    await wait(expect(axios.get).toHaveBeenCalledWith('http://localhost:5000/usuarios?nome_like=foo'));

    expect(await findByText('foo bar')).toBeInTheDocument();
    expect(await findByText('213.123.123-45')).toBeInTheDocument();
    expect(await findByText('foo_bar@email.com')).toBeInTheDocument();
    expect(await findByText('cidade eterna')).toBeInTheDocument();
    expect(await findByTestId('edit-icon')).toBeInTheDocument();
    expect(await findByTestId('trash-icon')).toBeInTheDocument();
  });
});

describe('After click Edit Icon ', () => {
  test('show form to change user infos and after use Voltar, the search input must be shown', async () => {
    axios.get = jest.fn().mockResolvedValue({
      data: [
        { 
          id: '1',
          nome: 'foo bar',
          cpf: '213.123.123-45',
          email: 'foo_bar@email.com',
          endereco: {
            cep: '13454000',
            rua: 'rua talvez',
            numero: '785',
            bairro: 'bairro azul',
            cidade: 'cidade eterna',
          }
        }
      ]
    });
    const { getByPlaceholderText, getByText, findByTestId } = render(<HomePage />);

    const editIcon = await findByTestId('edit-icon');

    userEvent.click(editIcon);

    expect(getByPlaceholderText('Nome')).toHaveValue('foo bar');
    expect(getByPlaceholderText('000.000.000-00')).toHaveValue('213.123.123-45');
    expect(getByPlaceholderText('exemplo@exemplo.com')).toHaveValue('foo_bar@email.com');
    expect(getByPlaceholderText('00000-000')).toHaveValue('13454000');
    expect(getByPlaceholderText('Rua / Av.')).toHaveValue('rua talvez');
    expect(getByPlaceholderText('Número')).toHaveValue(785);
    expect(getByPlaceholderText('Bairro')).toHaveValue('bairro azul');
    expect(getByPlaceholderText('Cidade')).toHaveValue('cidade eterna');

    userEvent.click(getByText('Voltar'));

    expect(getByPlaceholderText(/pesquise/i)).toBeInTheDocument();
  });

  test(' change inputs and submit, after that the search input must be shown', async () => {
    axios.get = jest.fn().mockResolvedValue({
      data: [
        { 
          id: '1',
          nome: 'foo bar',
          cpf: '213.123.123-45',
          email: 'foo_bar@email.com',
          endereco: {
            cep: '13454000',
            rua: 'rua talvez',
            numero: '785',
            bairro: 'bairro azul',
            cidade: 'cidade eterna',
          }
        }
      ]
    });
    axios.put = jest.fn().mockResolvedValue();

    const { getByPlaceholderText, getByText, findByTestId } = render(<HomePage />);

    const editIcon = await findByTestId('edit-icon');

    userEvent.click(editIcon);

    const nameInput = getByPlaceholderText('Nome');

    await userEvent.type(nameInput, 'good one');

    expect(nameInput).toHaveValue('good one');

    const submitButton = getByText('Salvar');

    userEvent.click(submitButton);

    await wait(expect(axios.put).toHaveBeenCalledWith('http://localhost:5000/usuarios/1', {
      nome: 'good one',
      cpf: '213.123.123-45',
      email: 'foo_bar@email.com',
      endereco: {
        cep: '13454000',
        rua: 'rua talvez',
        numero: '785',
        bairro: 'bairro azul',
        cidade: 'cidade eterna',
      }
    }));
  });
});

describe('When click Trash button ', () => {
  test('line must be deleted', async () => {
    axios.get = jest.fn().mockResolvedValue({
      data: [
        { 
          id: '1',
          nome: 'foo bar',
          cpf: '213.123.123-45',
          email: 'foo_bar@email.com',
          endereco: {
            cep: '13454000',
            rua: 'rua talvez',
            numero: '785',
            bairro: 'bairro azul',
            cidade: 'cidade eterna',
          }
        }
      ]
    });
    axios.delete = jest.fn().mockResolvedValue();

    const { findByTestId } = render(<HomePage />);

    const trashButton = await findByTestId('trash-icon');

    userEvent.click(trashButton);

    await wait(expect(axios.delete).toHaveBeenCalledWith('http://localhost:5000/usuarios/1'));
  });
});
