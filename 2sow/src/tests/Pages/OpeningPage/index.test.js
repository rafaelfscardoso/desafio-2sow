import React from 'react';
import { MemoryRouter } from "react-router-dom";
import { render } from '@testing-library/react';
import userEvent from "@testing-library/user-event";

import OpeningPage from '../../../Pages/OpeningPage';

const mockHistoryPush = jest.fn();

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe('Initial render ', () => {
  test('must appear UPDAATER', () => {
    const { getByText } = render(<OpeningPage />);

    expect(getByText('UPDAATER')).toBeInTheDocument();
  });

  test('after 4s must go to LoginPage', () => {
    render(
      <MemoryRouter>
        <OpeningPage />
      </MemoryRouter>
    );

    setTimeout(() => {
      expect(mockHistoryPush).toHaveBeenCalledWith('/login');
    }, 4000);
  });
});