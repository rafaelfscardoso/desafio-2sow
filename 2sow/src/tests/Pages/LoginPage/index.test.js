import React from 'react';
import { MemoryRouter } from "react-router-dom";
import { render } from '@testing-library/react';
import userEvent from "@testing-library/user-event";

import LoginPage from '../../../Pages/LoginPage';

const mockHistoryPush = jest.fn();

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe('Initial render,', () => {

  beforeEach(() => {
    Object.defineProperty(window, 'localStorage', {
      value: {
        getItem: jest.fn(() => 'token'),
        setItem: jest.fn(() => null),
        removeItem: jest.fn(() => null)
      },
      writable: true
    });
  });

  test('if token exists, must go to HomePage', () => {
    render(<LoginPage />);

    expect(window.localStorage.getItem).toHaveBeenCalledTimes(1);
    expect(mockHistoryPush).toHaveBeenCalledWith('/home');
  });

  test('must have email input', () => {
    const { getByPlaceholderText } = render(<LoginPage />);

    const emailInput = getByPlaceholderText(/exemplo/i);

    expect(emailInput).toBeInTheDocument();
  }); 

  test('must have password input', () => {
    const { getByPlaceholderText } = render(<LoginPage />);

    const passwordInput = getByPlaceholderText(/4 carac/i);

    expect(passwordInput).toBeInTheDocument();
  });

  test('must have a icon \'eye slash\'', () => {
    const { getByTestId } = render(<LoginPage />);

    const showPasswordIcon = getByTestId('showPassword-icon');

    expect(showPasswordIcon).toBeInTheDocument();
    expect(showPasswordIcon).toHaveClass('eye slash');
  });

  test('must have submit button', () => {
    const { getByText } = render(<LoginPage />);

    const submitButton = getByText(/confirmar/i);

    expect(submitButton).toBeInTheDocument();
  });
});

describe('When type ', () => {
  test('email must change', async () => {
    const { getByPlaceholderText } = render(<LoginPage /> );

    const emailInput = getByPlaceholderText(/exemplo/i);

    await userEvent.type(emailInput, 'teste@email.com');

    expect(emailInput).toHaveValue('teste@email.com');
  });

  test('password must change', async () => {
    const { getByPlaceholderText } = render(<LoginPage /> );

    const passwordInput = getByPlaceholderText(/4 carac/i);

    await userEvent.type(passwordInput, 'senhaTeste');

    expect(passwordInput).toHaveValue('senhaTeste');
  });

  test(' and click button \'show password\' must change icon', () => {
    const { getByTestId } = render(<LoginPage />);

    const showPasswordIcon = getByTestId('showPassword-icon');

    userEvent.click(showPasswordIcon);
    expect(showPasswordIcon).not.toHaveClass('slash');
    expect(showPasswordIcon).toHaveClass('eye');
  });
});

describe('When submit form' , () => {

  beforeEach(() => {
    Object.defineProperty(window, 'localStorage', {
      value: {
        getItem: jest.fn(() => null),
        setItem: jest.fn(() => null),
        removeItem: jest.fn(() => null)
      },
      writable: true
    });
  });

  test('and email is invalid', async () => {
    const { getByPlaceholderText, getByText } = render(<LoginPage />);

    const emailInput = getByPlaceholderText(/exemplo/i);
    const passwordInput = getByPlaceholderText(/4 carac/i);

    const submitButton = getByText(/confirmar/i);

    await userEvent.type(emailInput, 'teste@email');
    await userEvent.type(passwordInput, 'senhaTeste');

    userEvent.click(submitButton);
    expect(getByText('Favor insira um email válido')).toBeInTheDocument();
  });

  test('and email is valid but password is not', async () => {
    const { getByPlaceholderText, getByText } = render(<LoginPage />);

    const emailInput = getByPlaceholderText(/exemplo/i);
    const passwordInput = getByPlaceholderText(/4 carac/i);

    const submitButton = getByText(/confirmar/i);

    await userEvent.type(emailInput, 'teste@email.com');
    await userEvent.type(passwordInput, 'sen');

    userEvent.click(submitButton);
    expect(getByText('A senha deve ter ao menos 4 caracteres')).toBeInTheDocument();
  });

  test('and inputs are valid', async () => {
    const { getByPlaceholderText, getByText } = render(
      <MemoryRouter>
        <LoginPage />
      </MemoryRouter>
    );

    const emailInput = getByPlaceholderText(/exemplo/i);
    const passwordInput = getByPlaceholderText(/4 carac/i);

    const submitButton = getByText(/confirmar/i);

    await userEvent.type(emailInput, 'teste@email.com');
    await userEvent.type(passwordInput, 'senhaTeste');

    userEvent.click(submitButton);
    expect(window.localStorage.setItem).toHaveBeenCalledWith('token', 'token');
    expect(mockHistoryPush).toHaveBeenCalledWith('/home');
  });
});

