**Desafio 2SOW - Cadastro e tabela de usuários**

O desafio consiste em desenvolver uma aplicação front-end em que tenha uma página com formulário para cadastro de usuários e outra com uma tabela mostrando os usuários já cadastrados. Em ambos os casos, é necessário se logar, que é realizado numa tela anterior informando um email válido e uma senha de ao menos 4 dígitos.

---

## Desenvolvimento

O back-end foi simulado usando a lib json-server.

O front-end foi feito utilizado o framework ReacJS, HTML e CSS. As libs externas usadas foram axios para poder interagir com a API, styled-components para poder estilizar a aplicação e Semantic-ui para utilizar os elementos de interação com o usuário.

A aplicação foi desenvolvida responsivamente, funcionando tanto em ambiente mobile quanto desktop.

Os testes foram feitos utilizando a react-testing-library, lib padrão que já vem instalada com React, e Jest.

---

## Execução

Depois que o repositório tiver sido clonado, é preciso estar no diretório que a aplicação foi desenvolvida, bastando rodar no terminal o comando cd ./2sow.

Agora precisa rodar o comando npm install (para instalar as dependências utilizadas localmente).

Para rodar os testes automatizados, rodar npm run coverage. Aparecerá uma tabela mostrando a cobertura dos testes, note que foi realizado em mais de 90% do código.

Para rodar a aplicação em si, preciso em uma saída do terminal rodar o comando npm run server para simular a conexão com o servidor e em outra saída do terminal rodar npm run start e depois abrir em um navegador o endereço http://localhost:3000/.